# Development Setup

You need to clone this repo first in your local environment.
After cloned, go to `docker` folder from root project directory, and run:

## docker-compose build 

**Note: Run this command only the first time**

It might take time. After finished, you can continue by run the command: 

## docker-compose up

You can see the docker container logs and ..
App is ready to Rock !

# Run and Develop Application 

## Run and Develop Laravel App

Runs the Laravel app needed to be done inside the docker container (in this case, we need to enter `laravel-app` container). Runs it by the command: 
`docker-compose exec laravel-app bash` in `docker` folder
If you already inside the container, make sure you already in root directory and runs some below:

- composer install
- chmod -R 777 boostrap/cache
- chmod -R 777 storage

**Note: Run those commands above only the first time**

Open [http://localhost:3600](http://localhost:3600) to view it in the browser.

The page needs to be reloaded when you do edit / develop the app!
